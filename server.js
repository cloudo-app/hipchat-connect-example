import bodyParser from "body-parser";
import express from "express";
import yargs from "yargs";

const app = express();

const paths = {
    root: "/",
    descriptor: "/descriptor/",
    installCallback: "/install-callback/",
    glance: "/glance/",
    sidebar: "/sidebar/",
    dialog: "/dialog/",
    icon: "/cloudo.png"
};

const getUrl = (name) => {
    if (paths[name]) {
        return yargs.argv.ngrok + paths[name];
    }

    return yargs.argv.ngrok;
};

app.use(bodyParser.json());

app.use(express.static("files"));

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    next();
});

app.get(paths.root, (req, res) => {
    res.send("<h1>Hey Ho Let's Go!</h1>");
});

app.post(paths.installCallback, (req, res) => {
    res.send("<h1>Install Callback</h1>");
});

app.get(paths.glance, (req, res) => {
    res.json({
        label: {
            "type": "html",
            "value": "<b>Hey Ho Let's Go!</b>"
        },
        metadata: {
            dummy: true
        }
    });
});

app.get(paths.sidebar, (req, res) => {
    res.send("<h3>Sidebar</h3>");
});

app.get(paths.dialog, (req, res) => {
    res.send("<h3>Dialog</h3>");
});

app.get("/descriptor/", (req, res) => {
    res.json({
        key: "hipchat-connect-example-cloudo",
        name: "Tutorial",
        description: "A simple add-on showing cool hipchat connect api features.",
        vendor: {
            name: "Juraj Pelikan",
            url: "https://www.cloudo.co"
        },
        links: {
            self: getUrl("descriptor")
        },
        capabilities: {
            hipchatApiConsumer: {
                scopes: [
                    "send_notification",
                    "view_room",
                    "view_group",
                    "view_messages"
                ]
            },
            installable: {
                allowGlobal: true,
                callbackUrl: getUrl("installCallback")
            },
            glance: [
                {
                    key: "hipchat-connect-example-cloudo.glance",
                    name: {
                        value: "Glance"
                    },
                    queryUrl: getUrl("glance"),
                    icon: {
                        url: getUrl("icon"),
                        "url@2x": getUrl("icon")
                    },
                    target: "hipchat-connect-example-cloudo.sidebar"
                }
            ],
            webPanel: [
                {
                    key: "hipchat-connect-example-cloudo.sidebar",
                    name: {
                        "value": "Today"
                    },
                    url: getUrl("sidebar"),
                    location: "hipchat.sidebar.right"
                }
            ],
            action: [
                {
                    key: "hipchat-connect-example-cloudo.action",
                    location: "hipchat.input.action",
                    name: {
                        value: "Open dialog"
                    },
                    target: "hipchat-connect-example-cloudo.dialog"
                }
            ],
            dialog: [
                {
                    key: "hipchat-connect-example-cloudo.dialog",
                    url: getUrl("dialog"),
                    options: {
                        size: {
                            width: "600px",
                            height: "400px"
                        }
                    },
                    title: {
                        value: "Dialog"
                    }
                }
            ]
        }
    });
});

app.listen(8000, () => {
    console.log(`Integration descriptor url: ${getUrl("descriptor")}`);
});
