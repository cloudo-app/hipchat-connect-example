import express from "express";
import yargs from "yargs";

const app = express();

const paths = {
    root: "/"
};

const getUrl = (name) => {
    if (paths[name]) {
        return yargs.argv.ngrok + paths[name];
    }

    return yargs.argv.ngrok;
};

app.get(paths.root, (req, res) => {
    res.send("<h1>Hey Ho Let's Go!</h1>");
});

app.listen(8000, () => {
    console.log(`${getUrl("root")}`);
});
