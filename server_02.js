import express from "express";
import yargs from "yargs";

const app = express();

const paths = {
    root: "/",
    descriptor: "/descriptor/",
};

const getUrl = (name) => {
    if (paths[name]) {
        return yargs.argv.ngrok + paths[name];
    }

    return yargs.argv.ngrok;
};

app.get(paths.root, (req, res) => {
    res.send("<h1>Hey Ho Let's Go!</h1>");
});

app.get("/descriptor/", (req, res) => {
    res.json({
        key: "hipchat-connect-example-cloudo",
        name: "Tutorial",
        description: "A simple add-on showing cool hipchat connect api features.",
        vendor: {
            name: "Juraj Pelikan",
            url: "https://www.cloudo.co"
        },
        links: {
            self: getUrl("descriptor")
        },
        capabilities: {
            hipchatApiConsumer: {
                scopes: [
                    "send_notification",
                    "view_room",
                ]
            }
        }
    });
});

app.listen(8000, () => {
    console.log(`Integration descriptor url: ${getUrl("descriptor")}`);
});
