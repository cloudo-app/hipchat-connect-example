import express from "express";
import bodyParser from "body-parser";
import yargs from "yargs";

const app = express();

const paths = {
    root: "/",
    descriptor: "/descriptor/",
    installCallback: "/install-callback/"
};

const getUrl = (name) => {
    if (paths[name]) {
        return yargs.argv.ngrok + paths[name];
    }

    return yargs.argv.ngrok;
};

app.use(bodyParser.json());

app.get(paths.root, (req, res) => {
    res.send("<h1>Hey Ho Let's Go!</h1>");
});

app.post(paths.installCallback, (req, res) => {
    console.log(req.body);
    res.send("OK");
});

app.get("/descriptor/", (req, res) => {
    res.json({
        key: "hipchat-connect-example-cloudo",
        name: "Tutorial",
        description: "A simple add-on showing cool hipchat connect api features.",
        vendor: {
            name: "Juraj Pelikan",
            url: "https://www.cloudo.co"
        },
        links: {
            self: getUrl("descriptor")
        },
        capabilities: {
            hipchatApiConsumer: {
                scopes: [
                    "send_notification",
                    "view_room"
                ]
            },
            installable: {
                allowGlobal: true,
                allowRoom: true,
                callbackUrl: getUrl("installCallback")
            }
        }
    });
});

app.listen(8000, () => {
    console.log(`Integration descriptor url: ${getUrl("descriptor")}`);
});
