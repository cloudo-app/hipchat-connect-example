import bodyParser from "body-parser";
import express from "express";
import yargs from "yargs";

const app = express();

const paths = {
    root: "/",
    descriptor: "/descriptor/",
    installCallback: "/install-callback/",
    glance: "/glance/",
    icon: "/cloudo.png"
};

const getUrl = (name) => {
    if (paths[name]) {
        return yargs.argv.ngrok + paths[name];
    }

    return yargs.argv.ngrok;
};

app.use(express.static("files"));

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    next();
});

app.get(paths.root, (req, res) => {
    res.send("<h1>Hey Ho Let's Go!</h1>");
});

app.post(paths.installCallback, (req, res) => {
    res.send("<h1>Install Callback</h1>");
});

app.get(paths.glance, (req, res) => {
    res.json({
        label: {
            "type": "html",
            "value": "<b>Hey Ho Let's Go!</b>"
        },
        metadata: {
            dummy: true
        }
    });
});

app.get("/descriptor/", (req, res) => {
    res.json({
        key: "hipchat-connect-example-cloudo",
        name: "Tutorial",
        description: "A simple add-on showing cool hipchat connect api features.",
        vendor: {
            name: "Juraj Pelikan",
            url: "https://www.cloudo.co"
        },
        links: {
            self: getUrl("descriptor")
        },
        capabilities: {
            hipchatApiConsumer: {
                scopes: [
                    "send_notification",
                    "view_room"
                ]
            },
            installable: {
                allowGlobal: true,
                callbackUrl: getUrl("installCallback")
            },
            glance: [
                {
                    key: "hipchat-connect-example-cloudo.glance",
                    name: {
                        value: "Glance"
                    },
                    queryUrl: getUrl("glance"),
                    icon: {
                        url: getUrl("icon"),
                        "url@2x": getUrl("icon")
                    }
                }
            ]
        }
    });
});

app.listen(8000, () => {
    console.log(`Integration descriptor url: ${getUrl("descriptor")}`);
});
